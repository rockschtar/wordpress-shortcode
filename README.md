# rockschtar/wordpress-shortcode

# Requirements

  - PHP 7.1+
  - [Composer](https://getcomposer.org/) to install

# License

rockschtar/wordpress-shortcode is open source and released under MIT license. See LICENSE file for more info.

# Usage

Create a shortcode class

    class TestShortcode extends AbstractShortcode {

        public function getShortcodeName(): string {
            return 'myshortcode';
        }

        public function display_shortcode($attributes): string {

            $parsed_attributes = wp_parse_args($attributes, ['foo' => 'bar']);
            $output = 'This is my shortcode ' . $parsed_attributes['foo'];
            return $output;
        }
    }

Initialize the class

    TestShortcode::init();


# Question? Issues?

rockschtar/wordpress-shortcode is hosted on GitLab. Feel free to open issues there for suggestions, questions and real issues.
