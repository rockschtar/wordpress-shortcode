<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Shortcode;

abstract class AbstractShortcode {
	private static $_instances;

	private function __construct() {
		add_action('init', array($this, 'addShortcode'));
		add_action('wp_head', array(&$this, 'wpHead'));
		add_action('wp_enqueue_scripts', array(&$this, '_enqueueScripts'));
		add_action('wp_ajax_get_shortcode_' . $this->getShortcodeName(), array($this, 'ajaxGetShortcode'));
	}

	abstract public function getShortcodeName(): string;

    abstract public function displayShortcode($attributes): string;

    public function wpHead(): void {

    }

    public function enqueueScripts(): void {

    }

    public function getAttributes(): ?array {
        return array();
    }

	final public static function getInstance(): AbstractShortcode {
		return self::init();
	}

	/**
	 * @return static
	 */
	final public static function &init() {
		$class = static::class;
		if(!isset(self::$_instances[ $class ])) {
			self::$_instances[ $class ] = new $class();
		}

		return self::$_instances[ $class ];
	}

	final public function _enqueueScripts(): void {
		global $post;

		if(!$post) {
			return;
		}

		if(has_shortcode($post->post_content, $this->getShortcodeName())) {
			$this->enqueueScripts();
		}
	}

	final public function addShortcode(): void {
		add_shortcode($this->getShortcodeName(), array($this, 'displayShortcode'));
	}

	final public function ajaxGetShortcode(): void {

		$attributes = filter_input(INPUT_GET, 'attributes', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);

		echo $this->getShortcode($attributes);

		wp_die();
	}

	final public function getShortcode(?array $attributes): string {
		$shortcode_pattern = $this->getPattern();

		if(empty($attributes)) {
			return $shortcode_pattern;
		}

		$shortcode = '';

		foreach($attributes as $key => $value) {
			$shortcode = str_replace('{' . $key . '}', $value, $shortcode_pattern);
		}

		return $shortcode;
	}

	final public function getPattern(): string {

		$attribute_pattern = '';

		foreach($this->getAttributes() as $attribute) {
			$attribute_pattern .= "{$attribute}=\"{{$attribute}}\" ";
		}

		if($attribute_pattern !== '') {
			$attribute_pattern = trim($attribute_pattern);
			$attribute_pattern = ' ' . $attribute_pattern;
		}

		$shortcode_pattern = "[{$this->getShortcodeName()}{attributes}]";
		$shortcode_pattern = str_replace('{attributes}', $attribute_pattern, $shortcode_pattern);

		return $shortcode_pattern;
	}
}
